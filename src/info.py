from time import sleep
from threading import Thread, Lock
from datetime import datetime, timedelta

from geocoder import Geocode

class Info():

    def __init__(self, geomed, time, celsius=None, fahrenheit=None, error=False):
        self.geomed = geomed
        self.time = time
        self.celsius = celsius
        self.fahrenheit = fahrenheit
        self.error = error

    def __str__(self):
        return str(vars(self))

    def __repr__(self):
        return self.__str__()

    def to_dict(self):
        return vars(self)


class Cache():

    def __init__(self, persistence, forecast, decimal_places, valid_period_seconds):
        self.persistence = persistence
        self.forecast = forecast
        self.__decimal_places = decimal_places
        self.__valid_period = timedelta(seconds=valid_period_seconds)

    def __get_cached_info(self, geomed):
        try:
            hash_id = geomed.__hash__()
            end = datetime.now()
            start = end - self.__valid_period
            time, celsius, fahrenheit, latitude, longitude = self.persistence.get_info(hash_id, start, end)
        except Exception as e:
            return None
        else:
            Geocode(latitude, longitude)
            return Info(geomed, time, celsius, fahrenheit)

    def __request_info(self, geomed):
        now = datetime.now()
        try:
            celsius, fahrenheit = self.forecast.get(geomed)
            hash_id, latitude, longitude = geomed.to_tuple()
            self.persistence.register_info(hash_id, now, celsius, fahrenheit, latitude, longitude)
        except Exception as e:
            print("Cannot complete info request: {}".format(e))
            return Info(geomed, now, error=True)
        else:
            return Info(geomed, now, celsius, fahrenheit)

    def __get_info(self, geomed):
        print("Getting: {}".format(geomed))
        cached = self.__get_cached_info(geomed)
        print("Cached {}".format(cached))
        if cached:
            return cached
        else:
            print("Creating: {}".format(geomed))
            return self.__request_info(geomed)

    def update_info(self, locations):
        for location in locations:
            geomed = location.get_geomed(self.__decimal_places)
            info = self.__get_info(geomed)
            location.add(info)
