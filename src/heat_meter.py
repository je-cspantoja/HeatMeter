from datetime import datetime, timedelta

from databaser import Databaser
from geocoder import G_Geocoder
from info import Cache
from forecast import Forecast
from service import Service
import config

def create_service():
    geocoder = G_Geocoder(config.GEOCODING_KEY)
    persistence = Databaser(config.DB_URL)
    forecast = Forecast(config.FORECAST_KEY)
    cache = Cache(
        persistence,
        forecast,
        config.GEOCODE_DECIMAL_PLACES,
        config.CACHE_EXPIRATION_SECONDS
    )
    return Service(persistence, geocoder, cache)
