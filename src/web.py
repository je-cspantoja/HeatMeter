import flask
from flask import request
from flask_cors import CORS
import json
import datetime
import geocoder
import info

class Server():

    def __init__(self, service):
        self.app = flask.Flask("HeatMeter", static_folder="web/")
        CORS(self.app)
        self.service = service

        self.__create_rules()

    def __call__(self, enviroment, start_response):
        return self.app(enviroment, start_response)

    def __create_rules(self):
        self.app.add_url_rule("/usage/", "usage", self.usage)
        self.app.add_url_rule("/forecast/<string:data>", "get_forecast", self.get_forecast)
        self.app.add_url_rule("/", "root1", self.root1)
        self.app.add_url_rule("/<path:file>", "root2", self.root2)

    def root1(self):
        return self.app.send_static_file('index.html')

    def root2(self, file):
        return self.app.send_static_file(file)

    def usage(self):
        try:
            result = self.service.usage()
            response = {
                'result': str(result)
            }
        except Exception as e:
            response = {
                'error': str(result)
            }

        return flask.jsonify(response)

    def get_forecast(self, data):
        ip = request.environ['REMOTE_ADDR']
        response = {}

        try:
            result = self.service.handle(data, ip)
            response = {
                'result': result
            }
        except Exception as e:
            response = {
                'error': str(result)
            }

        return json.dumps(response, cls=_CustomEncoder)

class _CustomEncoder(json.JSONEncoder):
    DATE_FORMAT = "%Y-%m-%d"
    TIME_FORMAT = "%H:%M:%S"

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {
                "_type": "datetime",
                "value": obj.strftime("%s %s" % (
                    self.DATE_FORMAT, self.TIME_FORMAT
                ))
            }

        elif isinstance(obj, geocoder.Location):
            return obj.to_dict()

        elif isinstance(obj, geocoder.Geocode):
            return obj.to_dict()

        elif isinstance(obj, info.Info):
            return obj.to_dict()

        return super(RoundTripEncoder, self).default(obj)
