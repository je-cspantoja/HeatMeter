import os

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

Base = declarative_base()


class Consult(Base):
    __tablename__ = 'consults'

    id = Column(Integer, primary_key=True)
    time = Column(DateTime, nullable=False)
    ip = Column(String(15), nullable=False)
    search = Column(String(255), nullable=False)   

    def __init__(self, time, ip, search):
        self.time = time
        self.ip = ip
        self.search = search

    def __repr__(self):
        return "Consult {}, {}, {}".format(self.time, self.ip, self.search)


class Local_Relation(Base):
    __tablename__ = 'locals_relation'

    id = Column(Integer, primary_key=True)

    consult_id = Column(Integer, ForeignKey('consults.id'))
    consult = relationship("Consult", backref='locals_relation', lazy=False)

    local_id = Column(BigInteger, ForeignKey('locals.hash'))
    local = relationship("Local", backref='locals_relation', lazy=False)

    info_id = Column(Integer, ForeignKey('infos.id'), nullable=True)
    info = relationship("Info", backref='locals_relation', lazy=False)

    def __repr__(self):
        return "Relation"


class Local(Base):
    __tablename__ = 'locals'

    hash = Column(BigInteger, primary_key=True)
    name = Column(String(255), nullable=False)
    latitude = Column(Float, nullable=False)
    longitude = Column(Float, nullable=False)  

    def __init__(self, hash_id, name, latitude, longitude):
        self.hash = hash_id
        self.name = name
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return "Local {}, {}, {}".format(self.name, self.latitude, self.longitude)


class Info(Base):
    __tablename__ = 'infos'

    id = Column(Integer, primary_key=True)
    hash = Column(BigInteger, nullable=False)
    time = Column(DateTime, nullable=False)
    celsius = Column(Float, nullable=False)
    fahrenheit = Column(Float, nullable=False)
    latitude = Column(Float, nullable=False)
    longitude = Column(Float, nullable=False)  

    def __init__(self, hash_id, time, celsius, fahrenheit, latitude, longitude):
        self.hash = hash_id
        self.time = time
        self.celsius = celsius
        self.fahrenheit = fahrenheit
        self.latitude = latitude
        self.longitude = longitude

    def __repr__(self):
        return "Info {}, {}, {}, {}, {}".format(self.time, self.celsius, self.fahrenheit, self.latitude, self.longitude)


class Databaser():

    def __init__(self, db_url):
        self.engine = create_engine(db_url)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        Base.metadata.create_all(self.engine)

    def register_info(self, hash_id, time, celsius, fahrenheit, latitude, longitude):
        info = Info(hash_id, time, celsius, fahrenheit, latitude, longitude)
        try:
            self.session.add(info)
            self.session.commit()
        except Exception as e:
            print("Problem setting info persistence: {}".format(e))
            self.session.rollback()
            raise Exception

    def get_info(self, hash_id, start, end):
        try:
            to_filter = and_(Info.hash==hash_id, Info.time<=end, Info.time>=start)
            info = self.session.query(Info).filter(to_filter).order_by(desc(Info.time)).limit(1).one()               
        except NoResultFound as e:
            print("There is no results for info '{}'.".format(hash_id))
            raise Exception
        else:
            return info.time, info.celsius, info.fahrenheit, info.latitude, info.longitude

    def get_local(self, hash_id):
        try:
            location = self.session.query(Local).filter(Local.hash==hash_id).one()
        except NoResultFound as e:
            print(e)
            print("There is no results for location: '{}'.".format(hash_id))
            raise Exception
        else:
            return location

    def register_consult(self, time, data, ip, locations):
        try:
            print("Setting consult persistence")
            consult = Consult(time, ip, data)
            self.session.add(consult)
            for location in locations:
                hash_id = location.geocode.__hash__()
                try:
                    print("Trying getting Local.")
                    local = self.get_local(hash_id)
                except Exception:
                    print("Local not found. Creating...")
                    local = Local(hash_id, location.full_name, location.geocode.latitude, location.geocode.longitude)
                    self.session.add(local)
                hash_id = location.info.geomed.__hash__()
                info = self.session.query(Info).filter(Info.hash==hash_id).order_by(desc(Info.time)).limit(1).one()
                
                relation = Local_Relation()
                relation.consult = consult
                relation.local = local
                relation.info = info
                self.session.add(relation)
            self.session.commit()
        except Exception as e:
            print("Problem setting consult persistence: {}".format(e))
            self.session.rollback()
            raise Exception
        else:
            print("Success setting counsult: {}->{}".format(ip, data))

    def get_ip_consult_count(self, ip):
        return self.session.query(Consult).filter(Consult.ip==ip).count()

    def get_distinct_consult_ip(self):
        res = self.session.query(Consult.ip).distinct().all()
        return [ip[0] for ip in res]

    def get_all_ips_consult_count(self):
        ips_count_dict = {}
        for ip in self.get_distinct_consult_ip():
            count = self.get_ip_consult_count(ip)
            ips_count_dict[ip] = count
        return ips_count_dict
