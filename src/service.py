from datetime import datetime
import re

class Reverse_Search():

    NORMALIZE_LIST = [":", "|", ";", "/"]

    pattern = "-?\d+[.,]\d+[{}\s]+-?\d+[.,]\d+".format("".join([":", "|", ";", "/"]))
    regex = re.compile(pattern)

    def resolve(string):
        if Reverse_Search.regex.match(string) is None:
            return False, string
        return True, Reverse_Search.normalize(string)

    def normalize(string):
        for ch in Reverse_Search.NORMALIZE_LIST:
            if ch in string:
                string = string.replace(ch, " ")
        string = string.replace(",", ".")
        return tuple(float(cood) for cood in string.split())

class Service():

    def __init__(self, persistence, geocoder, cache):
        self.persistence = persistence
        self.cache = cache
        self.geocoder = geocoder

    def register_consult(self, data, ip, locations):
        try:
            time = datetime.now()
            self.persistence.register_consult(time, data, ip, locations)
        except Exception as e:
            print("Consult not registered. {}".format(e))
            print("  Responding request anyways.")

    def usage(self):
        return self.persistence.get_all_ips_consult_count()

    def handle(self, data, ip):
        reverse, request = Reverse_Search.resolve(data)
        locations = self.geocoder.get(request, reverse)
        if not self.geocoder.had_success():
            raise IOError("Not succeded in location requisition.")
        self.cache.update_info(locations)
        self.register_consult(data, ip, locations)
        return locations
