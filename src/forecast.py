from info import Info
from datetime import datetime

from forecastiopy import *

class Forecast():

    def __init__(self, key):
        self.key = key
    
    def __celsius2fahrenheit(self, celsius):
        return celsius * 9 / 5 + 32

    def __request_current(self, latitude, longitude):
        fio = ForecastIO.ForecastIO(self.key,
                        units=ForecastIO.ForecastIO.UNITS_SI,
                        lang=ForecastIO.ForecastIO.LANG_ENGLISH,
                        latitude=latitude, longitude=longitude)
        return FIOCurrently.FIOCurrently(fio)

    def get(self, geomed):
        try:
            current = self.__request_current(geomed.latitude, geomed.longitude)
            celsius = current.temperature
            fahrenheit = self.__celsius2fahrenheit(celsius)
        except Exception as e:
            print("Error build weather info: {}".format(e))
            raise AttributeError
        else:
            return celsius, fahrenheit
