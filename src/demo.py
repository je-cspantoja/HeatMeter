import heat_meter

def main():
    service = heat_meter.create_service(False)
    res = service.handle("88111520") # Normal form
    print(res)
    res = service.handle("-27.5656585 -48.6293934") # Same search by geocode
    print(res)
    res = service.handle("-27,5656585 -48,6293934") # Same than last
    print(res)
    res = service.handle("-27,5656585 | -48,6293934") # Same than last
    print(res)
    res = service.handle("-27,5656585|-48,6293934") # Same than last
    print(res)
    res = service.handle("-27,5656585/-48,6293934") # Same than last
    print(res)

main()