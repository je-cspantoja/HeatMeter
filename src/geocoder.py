import googlemaps

class Geocode():

    def __init__(self, latitude, longitude, _round=None):
        self.latitude = float(latitude)
        self.longitude = float(longitude)
        if type(_round) is int and _round >= 0:
            self.latitude = round(self.latitude, _round)
            self.longitude = round(self.longitude, _round)

    def get_geomed(self, decimal_places):
        return Geocode(self.latitude, self.longitude, decimal_places)

    def to_tuple(self):
        return self.__hash__(), self.latitude, self.longitude

    def __hash__(self):
        return hash((self.latitude, self.longitude))

    def __eq__(self, other):
        return self.__hash__() == other.__hash__()

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return str(vars(self))

    def __repr__(self):
        return self.__str__()

    def to_dict(self):
        return vars(self)


class Location():

    def __init__(self, full_name, geocode):
        self.full_name = full_name
        self.geocode = geocode
        self.info = None

    def add(self, info):
        self.info = info

    def get_geomed(self, decimal_places):
        return self.geocode.get_geomed(decimal_places)

    def __str__(self):
        return str(vars(self))

    def __repr__(self):
        return self.__str__()

    def to_dict(self):
        return vars(self)


class G_Geocoder():

    def __init__(self, key):
        self.__geocoder = googlemaps.Client(key=key)
        self.__success = True

    def __build_locations(self, locations_data):
        results = []
        for location_data in locations_data:
            try:
                full_name = location_data['formatted_address']
                latitude = location_data['geometry']['location']['lat']
                longitude =  location_data['geometry']['location']['lng']
                geocode = Geocode(latitude, longitude)
                location = Location(full_name, geocode)
            except Exception as e:
                print("Error building location: {}".format(e))
            else:
                results.append(location)
        return results

    def had_success(self):
        return self.__success

    def get(self, data, reversed=False):
        try:
            if reversed:
                locations_data = self.__geocoder.reverse_geocode(data)
            else:
                locations_data = self.__geocoder.geocode(data)
        except Exception as e:
            self.__success = False
            print("Could not get locations: {}".format(e))
            return []
        else:
            self.__success = True
            return self.__build_locations(locations_data)
