# Heat Meter

## This is a project to get forecast info by web

**All the commands listed in this manual assumes that you are in the root project directory**

#####################################################

**To install you can just execute:**
<p>`./install`

#####################################################

**To run with DB in memory you can just execute:**
<p>`gunicorn --config gunicorn.conf --pythonpath src main:server`

#####################################################

**Gunicorn accept more than 1 worker to handle HTTP requisitions from clients**
<p>Just set the argument in command above **--workers N_OF_WORKERS**
<p>or
<p>Adjust the configuration file of docker

#####################################################

**To change the DB you can set the DB_URL environment variable:**
  <p>`export DB_URL="postgresql://heat_meter:heat_meter@db/heat_meter" # runs with postgress if setuped`
  <p>`export DB_URL="sqlite:///app.db" # runs with python default sqlite`

#####################################################

**Dependencies to run with docker image of postgres within this project:**
  * docker
  * docker-compose

#####################################################

**If you met the dependencies listed above, you can just run**
  `docker-compose up`

#####################################################

**To configure docker you can edit "docker-compose.yml" file before run it**

#####################################################

**Default port to HTTP Server Gunicorn:**
<p>8080

#####################################################

**After run the program you can access the gui in your web browser with:**
<p>ip:8080 or localhost:8080

#####################################################

**Routes of interest:**
  * **/ :** default static page, returns index.html
  * **/index.html:** same as above
  * **/forecast/"params" :** the forecast consult
  * **/usage:** map of {ip : count} of usage statistic from ip

#####################################################

**Features:**
  * More than 1 worker
  * Cache in DB
  * Dump of DB 1 time for day
  * Get forecast info
  * Support to multiple DBs by abstraction layer provided by SQLAlchemy
  * Get usage info by web route

#####################################################